<?php

//logout.php

include('google/goog.php');
include('fablogin/config.php');
//Reset OAuth access token
$google_client->revokeToken();

//Destroy entire session data.
session_destroy();

//redirect page to index.php
header('location:index.php');

?>