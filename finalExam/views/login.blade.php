<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <title>Login</title>
</head>
<body>
    <div class="container">
    <div class="row d-flex justify-content-center align-items-center h-80">
      <div class="col-md-8 col-xl-9" style="padding-top:30px ;">
        <div class="card text-black" style="border-radius: 25px;height: 500px;">
          <div class="card-body p-sm-5">
            <div class="row justify-content-center">
              <div class="">
              
                <p class="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">supply office system<br>Login</p>

                <form class="mx-1 mx-md-2" method="post" action="/UserHome">    

                <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                  <div class="d-flex flex-row align-items-center mb-4">
                    <i class="fas fa-envelope fa-lg me-3 fa-fw"></i>
                    <div class="form-outline flex-fill mb-0">
                      <input type="text" id="form3Example3c" class="form-control" name="username" required/>
                      <label class="form-label" for="form3Example3c">Username</label>
                      
                    </div>
                  </div>

                  <div class="d-flex flex-row align-items-center mb-4">
                    <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                    <div class="form-outline flex-fill mb-0">
                      <input type="password" id="form3Example4c" class="form-control" name="password"  />
                      <label class="form-label" for="form3Example4c">Password</label>
                      
                    </div>
                  </div>                

                  <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                    <button type="submit" class="btn btn-primary btn-lg">Login</button>
                  </div>
                  <p class="sign-up">Don't have an Account?<a href="/register"> Sign Up</a></p>
                </form>

              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>
</body>
</html>