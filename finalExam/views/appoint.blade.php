<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <title>Psychologist</title>

</head>
<body>
    <div class="container mt-4">
    <nav class="navbar bg-info">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1">supply office system</span>
    <ul class="nav justify-content-end">
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/UserHome">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/appoint">Products</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/login">Logout</a>
  </li>
    </ul>
  </div>
</nav>

<div>
  

 <div class="py-5">
  <div class="container">
    <div class="row g-3">
    @foreach ($name as $prod)
      <div class="col-md-3 mt-3">
        <div class="card p-2 border-secondary mb-2" >
          <div class="card-body p-1">
            <h5 class="card-title text-center text-primary"> {{ $prod->name }}</h5>
            <h6>${{$prod->price}}</h6>
            <b>Stocks: {{ $prod->stocks}}</b><br>
          </div>
          <button type="button" class="btn btn-success" id="btn">Avail</button>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>
</div>

<script>
  const btn = document.getElementById('btn');

btn.addEventListener('click', function onClick() {
  btn.style.backgroundColor = 'salmon';
  btn.style.color = 'white';
});
</script>
</body>
</html>