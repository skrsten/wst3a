<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <title>Home</title>

</head>
<body>
    <div class="container mt-4">
    <nav class="navbar bg-info">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1">supply office system</span>
    <ul class="nav justify-content-end">
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/adminpage">Home</a>
  </li>
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/product">Product list</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/admin">Logout</a>
  </li>
    </ul>
  </div>
</nav>
<h2 align='center'>Add New Product</h2><br/>
      @if(Session::has('success'))
                  <div class="alert alert-success">{{Session::get('success')}}</div>
                  @endif
                  @if(Session::has('error'))
                  <div class="alert alert-danger">{{Session::get('error')}}</div>
                  @endif
      <form method="post" action="{{url('/prod_insert')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name">Name:</label>
            <input type="text" class="form-control" name="prod" required="">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="Number">Price:</label>
              <input type="text" class="form-control" name="price" required="">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="sched">Stock/s:</label>
              <input type="text" class="form-control" name="stock" required="">
            </div>
          </div>  
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4" style="margin-top:10px">
            <button type="submit" class="btn btn-success">Submit</button>
          </div>
        </div>
      </form>
<div>
</div>

</body>


</html>