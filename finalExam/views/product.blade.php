<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <title>Home</title>

</head>
<body>
    <div class="container mt-4">
    <nav class="navbar bg-info">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1">supply office system</span>
    <ul class="nav justify-content-end">
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/adminHome">Home</a>
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/product">Product List</a>
  </li>
  </li>
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/admin">Logout</a>
  </li>
    </ul>
  </div>
</nav>

<div>
<button type="button" class="btn btn-info mt-5" onclick="window.location.href='/prod_insert'">Add Product</button></td>
<table id="dtable" class="hover stripe cell-border"  style="width:100%">
<thead class="text-center">
      <tr>
        <th>ID</th>
        <th>Products</th>
        <th>Price</th>
        <th>Stock</th>
        <th>Date & Time</th>
        <th></th>
        <th></th>
        
      </tr>
    </thead>
    
    <tbody class="text-center">
    @foreach ($name as $prod)
      <tr>
      <td>{{ $prod->id }}</td>
      <td>{{ $prod->name }}</td>
      <td>{{ $prod->price}}</td>
      <td>{{ $prod->stocks}}</td>
      <td>{{ $prod->date}}</td>
      <td class="text-center"><button type="button" class="btn btn-warning" onclick="window.location.href='/prod_update/{{ $prod->id }}'">Edit</button></td>
      <td class="text-center"><button type="button" class="btn btn-danger" onclick="window.location.href='deleted/{{ $prod->id }}';return confirm('Are you sure you want to delete this item?');">Delete</button></td>
      
      </tr>
      @endforeach
    </tbody>
  </table>


</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
<script>
     $(document).ready( function () {
    $('#dtable').DataTable({
        pagingType: 'full_numbers',
         "lengthMenu": [5,10, 25, 50, 100],
    });
    
    
} );
</script>
</body>


</html>