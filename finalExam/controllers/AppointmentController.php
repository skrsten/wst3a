<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AppointmentController extends Controller
{
    

  public function insertform() {
        return view('register');
        }

    public function register(Request $request)
    {
        
        $email = $request->input('email');
        $username = $request->input('username');
        $password = $request->input('password');
        $confirm_pass = $request->input('confirm_pass');

        $data = [
            ['email'=> $email, 'username' => $username, 'password' => $password,'confirm_pass'=>$confirm_pass],
        ];
        
        DB::table('users')->insert($data); 
        return view('UserHome')->with('success', "Account successfully registered.");           
    }

    public function login() {
        return view('login');
        }

    public function loginUser() {
       $user = DB::select('select * from users');
       return view('UserHome',['users'=>$user]);
    }
    public function log() {
        return view('admin');
        }
    public function loginAdmin() {
        $admin = DB::select('select * from admin');
        return view('adminHome',['admin'=>$admin]);
     }

    public function Uhome() {
        return view('UserHome');
        }

    
            public function deleted() {
                $name = DB::select('select * from sched');
                return view('product',['name'=>$name]);
                    }
        
            public function destroyed($id) {
                DB::delete('delete from sched where id = ?',[$id]);
                return back()->with('success', "Deleted Successfully");     
                    }
     
    public function logout(Request $request) {
        Auth::logout();
        return redirect('login');
            }

    // public function approved($id){
    //     $update = DB::table('appoint')
    //                 ->where('id',$id)
    //                 ->update(["status"=>"Appointment Approved"]);
    //     return back();
    // }

    
    // public function occupied($id){
    //     $update = DB::table('appoint')
    //                 ->where('id',$id)
    //                 ->update(["status"=>"Time slot is occupied, appoint another one"]);
    //     return back();
    // }
}

