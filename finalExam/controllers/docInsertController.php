<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class docInsertController extends Controller
{
    public function create() {
        return view('prod_insert');
        }

    public function insert(Request $request)
    {
        $name = $request->input('prod');
        $price = $request->input('price');
        $stock= $request->input('stock');
        $date = $request->input('date');

        $data = [
            ['name'=>$name, 'price' => $price, 'stocks' => $stock , 'date' => $date],
        ];
        
        DB::table('sched')->insert($data); 
        return back()->with('success', "Successfully added new product");           
    }
}
