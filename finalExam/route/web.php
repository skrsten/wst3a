<?php

use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\schedController;
use App\Http\Controllers\viewController;
use App\Http\Controllers\patientController;
use App\Http\Controllers\docInsertController;
use App\Http\Controllers\doctorController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
  return view('welcome');
});




    //appointment system

    //register
    Route::get('register',[AppointmentController::class, 'insertform']);
    Route::post('register',[AppointmentController::class, 'register']);
    //login
    //Route::get('login',[AppointmentController::class, 'login']);
    Route::get('login',[AppointmentController::class, 'login']);
    Route::post('UserHome',[AppointmentController::class, 'loginUser']);
    Route::get('admin',[AppointmentController::class, 'log']);
    Route::post('adminHome',[AppointmentController::class, 'loginAdmin']);

    //home
    Route::get('appoint',[AppointmentController::class, 'home']);
    //insert
    Route::get('create',[schedController::class, 'create']); 
    Route::post('create',[schedController::class, 'insert']);

    Route::get('prod_insert',[docInsertController::class, 'create']); 
    Route::post('prod_insert',[docInsertController::class, 'insert']);

    //Route::get('appoint',[viewController::class, 'home']);
    Route::get('appoint',[viewController::class, 'index']);
    Route::get('myappointment',[viewController::class, 'client']);
    Route::get('adminpage',[patientController::class, 'patient']);
    Route::get('product',[patientController::class, 'product']);

    //edit 
    Route::get('doctor_update',[doctorController::class, 'index']);
    Route::get('prod_update/{id}',[doctorController::class, 'show']);
    Route::post('prod_update/{id}',[doctorController::class, 'edit']);

     //delete
     //Route::get('product',[AppointmentController::class, 'deleteS']);
     //Route::get('delete/{id}',[AppointmentController::class, 'destroyS']);
     Route::get('product',[AppointmentController::class, 'deleted']);
     Route::get('deleted/{id}',[AppointmentController::class, 'destroyed']);

     Route::get('myappointment',[AppointmentController::class, 'delete']);
     Route::get('delete/{id}',[AppointmentController::class, 'destroy']);
     //Route::get('myappointment',[doctorController::class, 'del']);
    // Route::get('delete/{id}',[doctorController::class, 'destroyS']);

     Route::get('/approved/{id}/',[AppointmentController::class, 'approved'])-> name('approved');
     Route::get('/occupied/{id}/',[AppointmentController::class, 'occupied'])-> name('occupied');