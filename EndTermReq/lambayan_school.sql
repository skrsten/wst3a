-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2022 at 12:20 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lambayan_school`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2a$12$WAb8m7d5YiFkZS/c8MB3Jesxp2/X5B4SGjz2jN2K9o2giqV8MkduS', '2022-05-21 08:47:22', '2022-06-01 03:38:16');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(3, 'Kristine O Soroten', 'sorotenkristine@gmail.com', 'sample message', '2022-06-06 21:39:01', '2022-06-06 21:39:01'),
(4, 'samplename', 'sample@email.com', 'this is a sample message', '2022-06-17 01:00:59', '2022-06-17 01:00:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2022_06_19_083438_create_admins_table', 0),
(2, '2022_06_19_083438_create_contacts_table', 0),
(3, '2022_06_19_083438_create_pages_table', 0),
(4, '2022_06_19_083438_create_posts_table', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `page-title` varchar(100) NOT NULL,
  `section_title` varchar(100) NOT NULL,
  `data` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `page-title`, `section_title`, `data`, `created_at`, `updated_at`) VALUES
(3, 'Home', 'second_title', 'LAMBAYAN ELEMENTARY SHOOL', '2022-06-02 22:08:24', '2022-06-12 01:21:17'),
(4, 'Home', 'second_text', '<h1><em>hey girl&nbsp;</em></h1>\r\n\r\n<h2 style=\"font-style:italic\"><strong>hfghfghf</strong></h2>', '2022-06-02 22:08:25', '2022-06-04 19:17:32'),
(5, 'Home', 'banner_image', '12062022165502567793.jpg', '2022-06-02 23:11:02', '2022-06-12 01:21:17'),
(6, 'Home', 'second_image', '03062022165424082390.png', '2022-06-02 23:20:24', '2022-06-02 23:20:24'),
(9, 'Faculty', 'second_title', 'FACULTY MEMBERS', '2022-06-04 02:13:44', '2022-06-17 01:01:47'),
(10, 'Faculty', 'second_image', '04062022165433763958.png', '2022-06-04 02:13:44', '2022-06-04 02:14:00'),
(11, 'Faculty', 'banner_image', '05062022165439907060.jpg', '2022-06-04 19:17:50', '2022-06-04 19:17:50'),
(12, 'About', 'second_title', 'ABOUT US', '2022-06-05 01:48:33', '2022-06-12 01:24:56'),
(13, 'About', 'banner_image', '12062022165502589692.jpg', '2022-06-05 01:48:33', '2022-06-12 01:24:56'),
(14, 'Blog', 'second_title', 'SCHOOL ACTIVITIES', '2022-06-05 04:51:25', '2022-06-05 05:05:05'),
(15, 'Blog', 'banner_image', '05062022165443430569.png', '2022-06-05 04:51:25', '2022-06-05 05:05:05');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `section_title` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `page_title`, `section_title`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(4, 'home', 'second_section', 'DepEd Mission', '<p>To protect and promote the right of every Filipino to quality, equitable, culture-based, and complete basic education where:</p>\r\n\r\n<p><strong>Students</strong>&nbsp;learn in a child-friendly, gender-sensitive, safe, and motivating environment.<br />\r\n<strong>Teachers</strong>&nbsp;facilitate learning and constantly nurture every learner.<br />\r\n<strong>Administrators and staff</strong>, as stewards of the institution, ensure an enabling and supportive environment for effective learning to happen.<br />\r\n<strong>Family, community, and other stakeholders</strong>&nbsp;are actively engaged and share responsibility for developing life-long learners.</p>', '05062022165439362550.png', '2022-06-04 05:21:14', '2022-06-04 17:47:05'),
(5, 'home', 'second_section', 'DepEd Vision', '<p>We dream of Filipinos<br />\r\nwho passionately love their country<br />\r\nand whose values and competencies<br />\r\nenable them to realize their full potential<br />\r\nand contribute meaningfully to building the nation.</p>\r\n\r\n<p>As a learner-centered public institution,<br />\r\nthe Department of Education<br />\r\ncontinuously improves itself<br />\r\nto better serve its stakeholders.</p>', '05062022165439391034.png', '2022-06-04 05:24:32', '2022-06-04 17:59:41'),
(7, 'home', 'second_section', 'Core Values', '<p><strong><em>Maka-Diyos<br />\r\nMaka-tao<br />\r\nMakakalikasan<br />\r\nMakabansa</em></strong></p>', '05062022165439536068.png', '2022-06-04 18:16:00', '2022-06-04 18:16:00'),
(19, 'faculty', 'slider', 'Principal', '<p><strong>Virgilio M.&nbsp;Mendoza&nbsp;</strong></p>\r\n\r\n<p><strong>Principal I</strong></p>', '06062022165449337235.png', '2022-06-04 23:29:24', '2022-06-05 21:29:32'),
(24, 'faculty', 'slider', 'Ungos', NULL, '06062022165451059086.jpg', '2022-06-04 23:37:38', '2022-06-06 02:16:30'),
(26, 'faculty', 'slider', 'Eparagas', NULL, '06062022165451060930.jpg', '2022-06-04 23:44:24', '2022-06-06 02:16:49'),
(27, 'faculty', 'slider', 'Dalope', NULL, '06062022165451065027.jpg', '2022-06-05 01:12:38', '2022-06-06 02:17:30'),
(28, 'faculty', 'slider', 'Lparagas', NULL, '06062022165451067317.jpg', '2022-06-05 01:15:37', '2022-06-06 02:17:53'),
(29, 'about', 'second_section', 'about', '<p><strong>Government organisation</strong></p>\r\n\r\n<p>&quot;A good teacher can inspire hope, ignite the imagination, and instill a love of learning.&quot; - Brad Henry Love</p>\r\n\r\n<p>A way of recognizing the hardworking and dedicated Lambayan Teaching Force. Continue to uplift the quality education and nurture the minds of the learners.</p>', '05062022165442524673.png', '2022-06-05 02:16:48', '2022-06-05 02:34:06'),
(30, 'blog', 'second_section', 'Formal opening of Progressive face-to-face classes', '<p><strong>May 16, 2022</strong></p>\r\n\r\n<hr />\r\n<p>At last, Face-to-face in our school is real! Let today be the start of something new. Welcome back, dear learners!</p>\r\n\r\n<p>Formal opening of Lambayan Elementary School Progressive face-to-face classes with Brgy. Captain Edwina V. Tambaoan and PTA President Leny F. Bonifacio.</p>\r\n\r\n<p>Thank you for gracing our momentous occasion.</p>', '05062022165443522533.jpg', '2022-06-05 05:08:18', '2022-06-08 22:37:03'),
(31, 'blog', 'second_section', 'Earth Day 2022', '<p><strong>April 27, 2022</strong></p>\r\n\r\n<hr />\r\n<p>Lambayan es Teachers and learners showed support in the celebration of Earth Day 2022 with the theme: <em><strong>Youth Invest in Our Planet.</strong></em></p>\r\n\r\n<p>Let&#39;s make the earth a better place to live. <strong>Let the earth breathe</strong>. <img alt=\"🌏\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t41/1/16/1f30f.png\" style=\"height:16px; width:16px\" /></p>\r\n\r\n<p>Sama-sama tayong kumilos para sa kalikasan! <img alt=\"✨\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/tf4/1/16/2728.png\" style=\"height:16px; width:16px\" /><img alt=\"🤝\" src=\"https://static.xx.fbcdn.net/images/emoji.php/v9/t64/1/16/1f91d.png\" style=\"height:16px; width:16px\" /></p>\r\n\r\n<p>depedkabataan-earthday2022</p>', '05062022165443459256.jpg', '2022-06-05 05:09:52', '2022-06-17 01:03:42'),
(32, 'faculty', 'slider', 'Burguillos', NULL, '06062022165451071038.jpg', '2022-06-06 02:18:30', '2022-06-06 02:18:30'),
(33, 'faculty', 'slider', 'Biala', NULL, '06062022165451072586.jpg', '2022-06-06 02:18:45', '2022-06-06 02:18:45'),
(34, 'faculty', 'slider', 'Caoile', NULL, '06062022165451075338.jpg', '2022-06-06 02:19:13', '2022-06-06 02:19:13'),
(35, 'faculty', 'slider', 'Lomibao', NULL, '06062022165451077086.jpg', '2022-06-06 02:19:30', '2022-06-06 02:19:30'),
(36, 'faculty', 'slider', 'Manuel', NULL, '06062022165451081980.jpg', '2022-06-06 02:20:19', '2022-06-06 02:20:19'),
(44, 'blog', 'second_section', 'Menstrual Hygiene (MH) Day', '<p><strong>May 27, 2022</strong></p>\r\n\r\n<hr />\r\n<p>Lambayan ES teaching force and learners watched&nbsp;<a href=\"https://www.facebook.com/hashtag/wearecommitted?__eep__=6&amp;__cft__[0]=AZXidwYCI1_xn0DiscQ6b7DNufQUsRAfM_mREBCGkRvmaLaAztRagsSpr_na7vGlJ_oxzcRlAaT1dKbsYN0K1mootxpY40kKrfTC1Y34XLHBJbTIVAZnBZxQrWQSbK7P0iZ2DVQTKBT66QmA4g7AKdxw4V0A9Fp-V5PhtNqdbWHPNg&amp;__tn__=*NK*F\">#WeareCommitted</a>. A webinar on <strong>Advancing Menstrual Health and Hygiene Management</strong> in the Philippines! In celebration of Menstrual Hygiene (MH) Day last May 27, 2022 .<br />\r\nDahil&nbsp;<a href=\"https://www.facebook.com/hashtag/oksadeped?__eep__=6&amp;__cft__[0]=AZXidwYCI1_xn0DiscQ6b7DNufQUsRAfM_mREBCGkRvmaLaAztRagsSpr_na7vGlJ_oxzcRlAaT1dKbsYN0K1mootxpY40kKrfTC1Y34XLHBJbTIVAZnBZxQrWQSbK7P0iZ2DVQTKBT66QmA4g7AKdxw4V0A9Fp-V5PhtNqdbWHPNg&amp;__tn__=*NK*F\">#OKsaDepEd</a>&nbsp;ang tamang pag&mdash;aalaga sa sarili</p>', '09062022165476898457.jpg', '2022-06-09 02:03:04', '2022-06-09 02:03:04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
