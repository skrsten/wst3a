<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Midterm - Customer
     
 
Route::get('/Customer/{CustomerID}/{Name}/{Address?}/{Age?}', function($CustomerID, $Name, $Address = NULL, $Age = NULL )

{

return "<head>
<link href=https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css rel=stylesheet integrity=sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3 crossorigin=anonymous>

<style>
td, th {
    border: 1px solid black;
    text-align: center;
    
  }

</style>

 <title>Customer</title>
</head>
 <br>
 <div class=container>
 <center><h1>CUSTOMER</h1> 
 <table class=table table-bordered border-primary>
 <thead>
   <tr>
     <th>CustomerID</th>
     <th>Name</th>
     <th>Address</th>
     <th>Age</th>
   </tr>
  </thead>

  <thead>
   <tr>
     <th>$CustomerID</th>
     <th>$Name</th>
     <th>$Address</th>
     <th>$Age</th>
   </tr>
  </thead>
</table>

<p><b>Kristine Soroten <br> BSIT-3A</p>";

});

//Midterm - Item
     
  
Route::get('/Item/{ItemNo}/{Name}/{Price}', function($ItemNo, $Name, $Price )

{

return "<head>
<link href=https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css rel=stylesheet integrity=sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3 crossorigin=anonymous>

<style>
td, th {
    border: 1px solid black;
    text-align: center;
    
  }

</style>

 <title>Item</title>
</head>
 <br>
 <div class=container>
 <center><h1>ITEM</h1> 
 <table class=table table-bordered border-primary>
 <thead>
   <tr>
     <th>ItemNo</th>
     <th>Name</th>
     <th>Price</th>
   </tr>
  </thead>

  <thead>
   <tr>
     <th>$ItemNo</th>
     <th>$Name</th>
     <th>$Price</th>
     
   </tr>
  </thead>
</table>

<p><b>Kristine Soroten <br> BSIT-3A</p>";

});

//Midterm - Order
     
  
Route::get('/Order/{CustomerID}/{Name}/{OrderNo}/{Date}', function($CustomerID, $Name, $OrderNo, $Date )

{

return "<head>
<link href=https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css rel=stylesheet integrity=sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3 crossorigin=anonymous>

<style>
td, th {
    border: 1px solid black;
    text-align: center;
    
  }

</style>

 <title>Order</title>
</head>
 <br>
 <div class=container>
 <center><h1>ORDER</h1> 
 <table class=table table-bordered border-primary>
 <thead>
   <tr>
     <th>CustomerID</th>
     <th>Name</th>
     <th>OrderNo</th>
     <th>Date</th>
   </tr>
  </thead>

  <thead>
   <tr>
     <th>$CustomerID</th>
     <th>$Name</th>
     <th>$OrderNo</th>
     <th>$Date</th>
     
   </tr>
  </thead>
</table>

<p><b>Kristine Soroten <br> BSIT-3A</p>";

});

//Midterm - Order Details
     
  
Route::get('/OrderDetails/{TransNo}/{OrderNo}/{ItemID}/{Name}/{Price}/{Qty}/{ReceiptNo?}', 
function($TransNo, $OrderNo, $ItemID, $Name, $Price, $Qty, $ReceiptNo = NULL)

{
$total = $Price*$Qty;
return "<head>
<link href=https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css rel=stylesheet integrity=sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3 crossorigin=anonymous>

<style>
td, th {
    border: 1px solid black;
    text-align: center;
    
  }

</style>

 <title>Order details</title>
</head>
 <br>
 <div class=container>
 <center><h1>ORDER DETAILS</h1> 
 <table class=table table-bordered border-primary>
 <thead>
   <tr>
     <th>TransNo</th>    
     <th>OrderNo</th>
     <th>ItemID</th>
     <th>Name</th>
     <th>Price</th>
     <th>Qty</th>
     <th>ReceiptNo</th>
     <th>Total</th>
   </tr>
  </thead>

  <thead>
   <tr>
     <th>$TransNo</th>
     <th>$OrderNo</th>
     <th>$ItemID</th>
     <th>$Name</th>
     <th>$Price</th>
     <th>$Qty</th>
     <th>$ReceiptNo</th>
     <th>$total</th>
     
     
   </tr>
  </thead>
</table>

<p><b>Kristine Soroten <br> BSIT-3A</p>";

});