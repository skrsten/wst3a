<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Bootstrap CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Portfolio</title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
    <!-- Box icon -->
    <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet">
    <!-- font awesome -->
    <script src="https://kit.fontawesome.com/ebc49af9c5.js" crossorigin="anonymous"></script>
    <!-- favicon -->
    <link href="{{ URL::asset('/images/favicon.png') }}" rel="icon">
    <!-- CSS -->
    <link rel="stylesheet" href="{{ URL::asset('/css/prtfl.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('/css/portfolio.css') }}">
</head>
<body>


    <div class="main-wrapper">
        <!-- Header -->
        <div class="header">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-light ">
                
  <div class="container">
    <a class="navbar-brand" href="portfolio/index">
        <img src="{{URL::asset('/images/logo.png')}}"  alt="" id="logo">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
        
        <li class="nav-item">
          <a class="nav-link" href="/about">About Me</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/contact">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/register">Register</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
      </ul>
   
    </div>
  </div>
</nav>
<!-- Navbar end -->
        </div>
        <!-- Header end-->
        <div class="container">
    

    <div class="information">
        <div class="about">            
        <div class="profile">
        <img src="images/pic1.png" alt="">
        <h3>Kristine Soroten</h3>
      
    </div>
            <div class="box-container">
                <div class="box">
                    <h3> <span>age : </span> 21 </h3>
                    <h3> <span> address : </span> Mapandan, Pangasinan </h3>
                    <h3> <span> number : </span> +123-456-7890 </h3>
                    <h3> <span> email : </span> sorotenkristine@gmail.com </h3>
                </div>              
            </div>
        </div>

 <div class="skills">
     <h3 class="title">Skills</h3>     
        <ul>
        <li>HTML</li>
        <li>CSS</li>
        <li>Editing</li>
        </ul>
 </div>

        <div class="experience">
            <h3 class="title">Education</h3>
            <div class="box-container">
                <div class="box">
                    <span>2019 - </span><br>
                    <span>Tertiary Education </span>
                    <h3>Bachelor of Science in Information Technology</h3>
                    <p>Pangasina State University-Urdaneta City Campus</p>
                </div>
                <div class="box">
                    <span>2013 - 2019</span>
                    <br>
                    <span>Secondary Education</span>
                    <h3>Junior-Senior High School</h3>
                    <p>Mapandan National High School</p>
                </div>
                <div class="box">
                    <span>2007 - 2013</span><br>
                    <span>Primary Education </span>
                    <h3>Elementary</h3>
                    <p>Mapandan Central School<br>
                        Lambayan Elementary School
                    </p>
                </div>
            </div>
        </div>

       

    </div>

</div>
    </div>

    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="social-media text-center">
                <ul class="footer-list">
                    <li><a href="https://www.facebook.com/"><i class='bx bxl-facebook'></i></a></li>
                    <li><a href="https://www.instagram.com/"><i class='bx bxl-instagram' ></i></a></li>
                    <li><a href="https://mail.google.com/"><i class='bx bxl-gmail'></i></a></li>
                    <li><a href="https://bitbucket.org/skrsten/wst3a/"><i class="fa-brands fa-bitbucket"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Footer end -->
</body>
</html>