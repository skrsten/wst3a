<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- Bootstrap CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Portfolio</title>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
    <!-- Box icon -->
    <link href="https://unpkg.com/boxicons@2.1.2/css/boxicons.min.css" rel="stylesheet">
    <!-- font awesome -->
    <script src="https://kit.fontawesome.com/ebc49af9c5.js" crossorigin="anonymous"></script>
    <!-- favicon -->
    <link href="{{ URL::asset('/images/favicon.png') }}" rel="icon">
    <!-- CSS -->
    <link rel="stylesheet" href="{{ URL::asset('/css/portfolio.css') }}">
</head>
<body>


    <div class="main-wrapper">
        <!-- Header -->
        <div class="header">
            <!-- Navbar -->
            <nav class="navbar navbar-expand-lg navbar-light ">
                
  <div class="container">
    <a class="navbar-brand" href="index">
        <img src="{{URL::asset('/images/logo.png')}}"  alt="" id="logo">
    </a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
        
        <li class="nav-item">
          <a class="nav-link" href="/about">About Me</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/contact">Contact</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/register">Register</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/login">Login</a>
        </li>
      </ul>
   
    </div>
  </div>
</nav>
<!-- Navbar end -->
        </div>
        <!-- Header end-->
    </div>
    <!-- Banner -->
    <div class="banner-section">
        <div class="container">
            <div class="row justify-content-center align-items-center">
                <div class="col-lg-6">
                    <h1 class="banner-title">
                        Hi! I'm Kristine Soroten 🙋‍♀️
                    </h1>
                    <p class="banner-desc">
                    A Bachelor of Science in Information Technology student at Pangasinan State University-Urdaneta City Campus.
                    </p>
                    <a href="/about" class="banner-btn">
                        read more &rAarr;
                    </a>
                </div>
                <div class="col-lg-6 col-md-8">
                <img src="{{URL::asset('/images/pic1.png')}}"  alt="" id="pic1">
                </div>
            </div>
        </div>
    </div>
    <!-- Banner end -->

    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <div class="social-media text-center">
                <ul class="footer-list">
                    <li><a href="https://www.facebook.com/"><i class='bx bxl-facebook'></i></a></li>
                    <li><a href="https://www.instagram.com/"><i class='bx bxl-instagram' ></i></a></li>
                    <li><a href="https://mail.google.com/"><i class='bx bxl-gmail'></i></a></li>
                    <li><a href="https://bitbucket.org/skrsten/wst3a/"><i class="fa-brands fa-bitbucket"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Footer end -->
</body>
</html>