1. 	Place the portfolio folder in the resources folder's views folder.
	This can be found in the laravel folder. (C:\xampp\htdocs\wst_blog\resources\views)

2. 	In the routes folder, add a route for the .blade.php files inside the portfolio
	folder in web.php.(C:\xampp\htdocs\wst_blog\routes) 
	--e.g--		
	Route::get('portfolio/index', function () {
    	return view('portfolio/index');
	});

	Route::get('/about', function () {
    	return view('portfolio/about');
	});

	Route::get('/contact', function () {
	    return view('portfolio/contact');
	});

	Route::get('/register', function () {
   	return view('portfolio/register');
	});

	Route::get('/login', function () {
    	return view('portfolio/login');
	});

	Route::get('/resume', function () {
    	return view('portfolio/resume');
	});

3.	In the public folder, place the css and images folder. (C:\xampp\htdocs\wst_blog\public)