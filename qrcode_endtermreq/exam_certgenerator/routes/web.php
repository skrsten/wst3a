<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CertController;
use Barryvdh\DomPDF\Facade\Pdf;
use Dompdf\Dompdf;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/findcertificates',[CertController::class, 'findcertificates']);

Route::get('/emailresent',[CertController::class, 'emailresent']);

Route::get('/deleteuser/{id}',[CertController::class, 'deleteuser']);
Route::get('/emailsent/{id}',[CertController::class, 'emailsent']);

Route::get('/index',[CertController::class, 'index_landing']);

Route::get('/',[CertController::class, 'index_landing']);

Route::get('/sample/{id}',[CertController::class, 'sample']);

Route::get('/editor',[CertController::class, 'editor_landing']);

Route::post('/certvalidate',[CertController::class, 'certvalidate']);


Route::get('/emailsent/{id}',[CertController::class, 'emailsent']);
Route::get('/download/{id}',[CertController::class, 'download']);


Route::get('/deleteseminar/{id}',[CertController::class, 'deleteseminar']);


Route::get('/login',[CertController::class, 'cert_login']);
Route::post('/check',[CertController::class, 'checkLogin']);
Route::get('/logout',[CertController::class, 'logout']);
Route::get('/viewCertificate/{id}',[CertController::class, 'viewCertificate']);
Route::post('/insertseminar',[CertController::class, 'insertseminar']);
Route::post('/insertGeneratedCerts',[CertController::class, 'insertGeneratedCerts']);
Route::post('/updateSeminar/{call?}',[CertController::class, 'updateSeminar']);
Route::post('/insertModules',[CertController::class, 'insertModules']);


Route::group(['middleware'=>['checkSession']], function(){

Route::get('/generatedcertificates',[CertController::class, 'generatedcerts']);

Route::get('/admin/seminars',[CertController::class, 'admin_editseminar']);

Route::get('/admin/seminars/{id}',[CertController::class, 'admin_editseminar_page']);

Route::get('/admin/seminars/update/{id}',[CertController::class, 'admin_editseminar_page_update']);

Route::get('/editor/editing',[CertController::class, 'editor_edit']);
Route::get('/admin/editing',[CertController::class, 'admin_edit']);

Route::get('/admin/editing/{id}',[CertController::class, 'admin_edit_page']);
Route::get('/editor/editing/{id}',[CertController::class, 'editor_edit_page']);

Route::get('/admin',[CertController::class, 'admin_landing']);
});