<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
  body{
    zoom: 110%;
  }
</style>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
      
      <script>
      $(document).ready(function () {
        $('#certificates').DataTable();
      });
      </script>

      <body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/admin">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">


                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/admin">Home</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/admin/editing">Edit Designs</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/admin/seminars">Edit Seminars</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/generatedcertificates">Generated Certificates</a>
                  </li>

                  <li class="nav-item">
                    <a href="/logout" type="button" class="p-0 m-0 btn btn-link btn-sm text-decoration-none mt-4">Logout</a>
                  </li>

                </ul>
              </div>
            </div>
          </nav>



    </div>

    <div class="container px-4">
      <div class="row mt-3">
          <div class="col-6">
  
          </div>
          <div class="col-6 text-end">
              <span class="badge text-bg-warning text-light">Logged as Admin</span>
          </div>
        
        </div>
    </div>

  <div class="container px-4 mt-3">

    @if(Session::get('success'))
      <div class="alert alert-success">
      {{ Session::get('success') }}
      </div>
      @endif

      @if(Session::get('fail'))
          <div class="alert alert-secondary">
          {{ Session::get('fail') }}
          </div>
      @endif


    <div class="rounded p-2">
      <h6>List of Generated Certificates</h6>
    </div>
  </div>

  <div class="container px-4 mt-3">
    <a href="/emailresent" class="btn btn-success">Resent All <i class="bi bi-send-fill"></i></a> 
  </div>


  <div class="container px-4 mt-3">
    <div class="shadow p-3 rounded">
    <table id="certificates" class="display" style="width:100%">
      <thead>
          <tr>
              <th>QR CODE</th>
              <th>Full Name</th>
              <th>Seminar</th>
              <th>Date Generated</th>
              <th>Status</th>
              <th>Date Sended</th>
              <th>Action</th>
          </tr>
      </thead>
      <tbody>
      
                  @foreach ($generated as $gen)

                  <tr>
                       <td><img width="50px" src="https://api.qrserver.com/v1/create-qr-code/?size=150x150&data={{ $gen->validation_code }}"></td>
                       <td>{{ $gen->firstname . " " . $gen->lastname  }}</td>
                       <td>{{ $gen->seminar_name }}</td>
                       <td>{{ $gen->date_generated }}</td>
                      <td>
                        @if($gen->status == 1)
                        <span class="badge bg-success">Sent</span>
                        @else

                        @endif
                      </td>
                      <td>{{ $gen->date_sended }}</td>

                      <td><a href="/viewCertificate/{{ $gen->generated_id }}" class="btn btn-primary"><i class="bi bi-eye-fill"></i></a> 
                        <a href="/deleteuser/{{ $gen->generated_id }}" class="btn btn-danger"><i class="bi bi-trash3-fill"></i></a> 
                       <a href="/emailsent/{{ $gen->generated_id }}" class="btn btn-success"><i class="bi bi-send-fill"></i></a> 
                      </td>  
                  </tr>

                  @endforeach
            
     
      </tbody>
  </table>
  </div>
</div>


    
</body>
</html>

