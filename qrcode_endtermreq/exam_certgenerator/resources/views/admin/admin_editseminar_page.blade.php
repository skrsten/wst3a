<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
  body{
    zoom: 110%;
  }
</style>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

<body>

    <div class="container">
        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/admin">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">


                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/admin">Home</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/admin/editing">Edit Designs</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/admin/seminars">Edit Seminars</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/generatedcertificates">Generated Certificates</a>
                  </li>

                  <li class="nav-item">
                    <a href="/logout" type="button" class="p-0 m-0 btn btn-link btn-sm text-decoration-none mt-4">Logout</a>
                  </li>



                </ul>
              </div>
            </div>
          </nav>
    </div>


    <div class="container px-4">
        <div class="row mt-3">
            <div class="col-6">
    
            </div>
            <div class="col-6 text-end">
                <span class="badge text-bg-warning">Logged as Admin</span>
            </div>
          
          </div>
    </div>



    <div class="container px-4 mt-3">
      @if(Session::get('success'))
      <div class="alert alert-warning">
      {{ Session::get('success') }}
      </div>
      @endif
  
      @if(Session::get('fail'))
          <div class="alert alert-secondary">
          {{ Session::get('fail') }}
          </div>
      @endif
    </div>


    <div class="container px-4 mt-4">

      <div class="fs-3 mb-4">Update <b>{{ $seminars[0]->seminar_name }}</b></div>
            <hr>
      <div class="shadow rounded p-3">
        <form action="/admin/seminars/update/{{  $seminars[0]->seminar_id}}/" method="get">
          {{ csrf_field() }}  
 
            <div class="row">
            <div class="col-12">
                Seminar Name:
                <input type="text" class="form-control" value="{{ $seminars[0]->seminar_name  }}" name="seminarname" id="seminarname">
            </div>
  
            <div class="col-12 mt-3">
                Seminar Description:
                <textarea class="form-control" value="{{ $seminars[0]->seminar_desc }}" name="seminardesc" id="seminardesc">{{ $seminars[0]->seminar_desc }}</textarea>
            </div>
          </div>
  
          <div>
              <button type="submit" class="btn btn-primary mt-4">Update Changes</button>
          </div>
        </form>
      </div>

     </div>


    
</body>
</html>

