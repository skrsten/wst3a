<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
  body{
    zoom: 110%;
  }
</style>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/admin">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">


                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/admin">Home</a>
                      </li>
    
                      <li class="nav-item">
                        <a class="nav-link" href="/admin/editing">Edit Designs</a>
                      </li>
    
                      <li class="nav-item">
                        <a class="nav-link" href="/admin/seminars">Edit Seminars</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="/generatedcertificates">Generated Certificates</a>
                      </li>

                      <li class="nav-item">
                        <a href="/logout" type="button" class="p-0 m-0 btn btn-link btn-sm text-decoration-none mt-4">Logout</a>
                      </li>




                </ul>
              </div>
            </div>
          </nav>
    </div>


    <div class="container px-4">
        <div class="row mt-3">
            <div class="col-6">
    
            </div>
            <div class="col-6 text-end">
                <span class="badge text-bg-warning">Logged as Admin</span>
            </div>
          
          </div>
    </div>

  


    <div class="container px-4 mt-4">

        <div class="fs-3 mb-4">Design <b>{{ $designs[0]->seminar_name }}</b></div>
      <hr>
        <div class="shadow rounded p-3">
          <form action="/updateSeminar/{{ $designs[0]->design_id }}" method="post">
            @csrf
        <div class="row">
            <input type="text" name="design" value="{{ $designs[0]->design_id }}" hidden>


            <div class="col-6">
              Base Design:
              <select name="base" id="base" class="form-control">
                  @foreach ($baseimage as $l)
                  <option class="fw-bold" value="{{ $l->baseimage_id }}">Current: {{ $l->baseimage_name }}</option>
                  @endforeach

                  @foreach ($baseimageall as $base)
                  <option value="{{ $base->baseimage_id }}">{{ $base->baseimage_name }}</option>
                  @endforeach
              </select>
            </div>
    
            <div class="col-6">
                Logo
                <select name="logo" id="logo" class="form-control">
                  @foreach ($logosingle as $l)
                  <option class="fw-bold" value="{{ $l->logo_id }}">Current: {{ $l->logo_name }}</option>
                  @endforeach

                    @foreach ($logoall as $logo)
                    <option value="{{ $logo->logo_id }}">{{ $logo->logo_name }}</option>
                    @endforeach

                    <option value="0">None</option>
                </select>
            </div>

           </div>


           
           <div class="row mt-4">


                <div class="col-6">
                    <div>Default Image:</div>
                    @foreach ($baseimage as $l)
                    <img width="70px" src='{{ asset($l->baseimage_img) }}' alt="">
                    @endforeach
                    <br>
                </div>

    
                <div class="col-6">
                    <div>Default Image:</div>
                    @foreach ($logosingle as $l)
                    <img width="70px" src={{ asset($l->logo_img) }} alt="">
                    @endforeach
                    <br>
                </div>

               
  
            </div>
        </div>


        <div class="col-2 mt-4">
          <select class="form-select" id="signature" name="signature">
            <option class="fw-bold" value="{{ count($signature) }}">Current: {{ count($signature) }} Signature</option>
            <option value="1">1</option>
            <option value="2">2</option>
          </select>
        </div>


        <div class="mt-2 rounded shadow p-3">

        
        <div class="row">


          <div class="col-6" id="signature0">
            Signature 1
            <select name="signature1" id="signature_id" class="form-control">

              @if(count($signature) == 1 || count($signature) == 2){
                <option class="fw-bold" value="{{ $signature[0][0]->signature_id }}">Current: {{ $signature[0][0]->signature_name.", " . $signature[0][0]->signature_position  }}</option>
              }
              @endif
                
                @foreach ($signatureall as $sign)
                <option value="{{ $sign->signature_id }}">{{ $sign->signature_name .", " . $sign->signature_position  }}</option>
                @endforeach
            </select>
         </div>
  
         <div class="col-6" id="signature1">
          Signature 2
          <select name="signature2" id="signature_id" class="form-control">

            @if(count($signature) == 2){
              <option class="fw-bold" value="{{ $signature[1][0]->signature_id }}">Current: {{ $signature[1][0]->signature_name.", " . $signature[1][0]->signature_position  }}</option>
            }
            @endif

              @foreach ($signatureall as $sign)
              <option value="{{ $sign->signature_id }}">{{ $sign->signature_name .", " . $sign->signature_position  }}</option>
              @endforeach
          </select>
          </div>


        </div>

        <div class="row mt-4">

          @if(count($signature) == 2)

          @php
            $count = 0;
          @endphp

            @foreach ($signature as $l)
            <div class="col-6" id="signatureimg{{$count}}">
                <div>Default Image:</div>
           
                <img width="70px" src={{ asset($signature[$count][0]->signature_img) }} alt="">
                @php
                $count++;
                @endphp
               
                <br>
            </div>
            @endforeach

          @elseif(count($signature) == 1)


          @endif


        </div>
      

        </div>




        <div>
          <button type="submit" class="btn btn-primary mt-4 float-end">Save Changes</button>
      </div>
      </form>
      
 
     </div>


</body>
</html>

<script>

$( document ).ready(function() {


      if($( "#signature option:selected" ).val() != 1){

          $("#signature0").show();
          $("#signatureimg0").show();  

          $("#signature1").show();
          $("#signatureimg1").show();  

      }else{

        $("#signature1").hide();
          $("#signatureimg1").hide();  

          $("#signature0").show();
          $("#signatureimg0").show();  

      }


      $( "#signature" ).change(function() {


      if($( "#signature option:selected" ).val() != 1){

          $("#signature0").show();
          $("#signatureimg0").show();  

          $("#signature1").show();
          $("#signatureimg1").show();  

      }else{

        $("#signature1").hide();
          $("#signatureimg1").hide();  

          $("#signature0").show();
          $("#signatureimg0").show();  

      }


      });

    });
</script>

