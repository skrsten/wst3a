
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Certificate Generator</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</head>
<body class="bg-dark">

    <div class="container">

        <div class="p-3 text-dark bg-light col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2 position-absolute top-50 start-50 translate-middle rounded">
            <div class="fw-bold">LOGIN</div>

            @if(empty($user))
            {{'No Session'}}
            @else
            {{'Has Session'}}
            @endif


            <p style="font-size: 10px">Certificate Generator</p>
            <hr>

            @if(Session::get('fail'))
            <div class="alert alert-danger">
            {{ Session::get('fail') }}
            </div>
            @endif

            <form action="check" method="post">
            @csrf
            <span style="font-size: 10px" class="text-danger">@error('username'){{ $message }} @enderror</span>
            <div class="my-2">
                <input type="text" aria-label="First name" name="username" class="form-control" placeholder="Username">
            </div>

            <span style="font-size: 10px" class="text-danger">@error('password'){{ $message }} @enderror</span>
            <div class="my-2">
                <input type="password" aria-label="First name" name="password" class="form-control" placeholder="Password">
            </div>

            <div class="d-grid gap-2 mt-3">
                <button class="btn btn-primary btn-sm" type="submit">Login</button>
            </div>

            </form>

        </div>
    </div>


    
</body>
</html>