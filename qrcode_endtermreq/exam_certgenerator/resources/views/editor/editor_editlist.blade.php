<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

<body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/editor">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/editor">Home</a>
                      </li>
    
                      <li class="nav-item">
                        <a class="nav-link" href="/editor/editing">Edit</a>
                      </li>

                </ul>
              </div>
            </div>
          </nav>
    </div>

    <div class="container px-4">
      <div class="row mt-3">
          <div class="col-6">
  
          </div>
          <div class="col-6 text-end">
              <span class="badge text-bg-success">Logged as Editor</span>
          </div>
        
        </div>
  </div>


    <div class="container px-4">

      <div class="row">

        @foreach ($designinfo as $design)

        <div class="col-3">
            <div class="card" style="width: 18rem;">
                <div class="card-body">
                <h5 class="card-title">{{ $design->seminar_name }}</h5>
                <p class="card-text">{{ $design->seminar_desc }}</p>
                <a href={{ '/editor/editing/' . $design->design_id }} class="btn btn-primary">Edit Design</a>
                </div>
            </div>
        </div>
  
      @endforeach

      </div>



     </div>


    <footer class="fixed-bottom p-4 mb-5">
        <div class="container">
            <a href="/logout" type="button" class="btn btn-link btn-sm text-decoration-none float-end mt-2">Logout</a>
        </div>

    </footer>
    
</body>
</html>

