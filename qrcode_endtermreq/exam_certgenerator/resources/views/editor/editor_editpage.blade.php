<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

<body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/editor">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">

                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/editor">Home</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/editor/editing">Edit</a>
                  </li>


                </ul>
              </div>
            </div>
          </nav>
    </div>


    <div class="container px-4">
        <div class="row mt-3">
            <div class="col-6">
    
            </div>
            <div class="col-6 text-end">
                <span class="badge text-bg-success">Logged as Editor</span>
            </div>
          
          </div>
    </div>


    <div class="container px-4 mt-4">

      <div class="fs-3 mb-4">{{ $designs[0]->seminar_name }}</div>

      <form action="/updateSeminar/editor" method="post">
          @csrf
      <div class="row">
        <input type="text" name="design" value="{{ $designs[0]->design_id }}" hidden>
  
          <div class="col-6">
              Logo
              <select name="logo" id="logo" class="form-control">
                  @foreach ($logoall as $logo)
                  <option value="{{ $logo->logo_id }}">{{ $logo->logo_name }}</option>
                  @endforeach
              </select>
          </div>
  
          <div class="col-6">
              Signature
              <select name="signature" id="signature_id" class="form-control">
                  @foreach ($signatureall as $sign)
                  <option value="{{ $sign->signature_id }}">{{ $sign->signature_name .", " . $sign->signature_position  }}</option>
                  @endforeach
              </select>
          </div>
         </div>

         <div class="row mt-4">
              <div class="col-6">
                  <div>Default Image:</div>
                  <img width="70px" src={{ asset($logo->logo_img) }} alt="">
                  <br>
              </div>
      
              <div class="col-6">
                  <div>Default Image:</div>
                  <img width="70px" src={{ asset($signature[0]->signature_img) }} alt="">
                  <br>
              </div>
          </div>


      <div class="float-end">
          <button type="submit" class="btn btn-primary mt-4">Save Changes</button>
      </div>
  </form>

   </div>


    <footer class="fixed-bottom p-4  mb-5">
        <div class="container">
            <a href="/logout" type="button" class="btn btn-link btn-sm text-decoration-none float-end mt-2">Logout</a>
        </div>

    </footer>
    
</body>
</html>

