-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2022 at 04:47 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `certificate_generator`
--

-- --------------------------------------------------------

--
-- Table structure for table `base_image`
--

CREATE TABLE `base_image` (
  `baseimage_id` int(11) NOT NULL,
  `baseimage_name` varchar(50) DEFAULT NULL,
  `baseimage_img` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `base_image`
--

INSERT INTO `base_image` (`baseimage_id`, `baseimage_name`, `baseimage_img`) VALUES
(8, 'DesignNew', '/images/DesignNew.png');

-- --------------------------------------------------------

--
-- Table structure for table `designs`
--

CREATE TABLE `designs` (
  `design_id` int(11) NOT NULL,
  `baseimage_id` int(11) DEFAULT NULL,
  `logo_id` int(11) DEFAULT NULL,
  `seminar_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `designs`
--

INSERT INTO `designs` (`design_id`, `baseimage_id`, `logo_id`, `seminar_id`) VALUES
(20, 8, 9, 34);

-- --------------------------------------------------------

--
-- Table structure for table `generated_certs`
--

CREATE TABLE `generated_certs` (
  `firstname` varchar(50) DEFAULT NULL,
  `validation_code` text DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `date_generated` datetime DEFAULT NULL,
  `generated_id` int(11) NOT NULL,
  `seminar_id` int(11) DEFAULT NULL,
  `email` text DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `date_sended` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `generated_certs`
--

INSERT INTO `generated_certs` (`firstname`, `validation_code`, `lastname`, `date_generated`, `generated_id`, `seminar_id`, `email`, `status`, `date_sended`) VALUES
('Ram Wendel', '$2y$10$HMSZ/vn0/OFrY4WyJqVms.nVV.PvYLI62E8xsZq88RrkMFzedEcgq', 'Dedomo', '2022-07-02 14:57:42', 48, 34, 'generatorcertificate@gmail.com', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_credentials`
--

CREATE TABLE `login_credentials` (
  `user_id` int(11) NOT NULL,
  `user_firstname` varchar(50) DEFAULT NULL,
  `user_lastname` varchar(50) DEFAULT NULL,
  `user_type` int(11) DEFAULT NULL,
  `user_username` varchar(50) DEFAULT NULL,
  `user_password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `login_credentials`
--

INSERT INTO `login_credentials` (`user_id`, `user_firstname`, `user_lastname`, `user_type`, `user_username`, `user_password`) VALUES
(1, 'Juan', 'Cruz', 0, 'admin', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
  `logo_id` int(11) NOT NULL,
  `logo_name` varchar(50) DEFAULT NULL,
  `logo_img` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`logo_id`, `logo_name`, `logo_img`) VALUES
(8, 'PSU', '/images/psulogo.png'),
(9, 'MAPANDAN', '/images/MAPANDAN-SEAL-.png');

-- --------------------------------------------------------

--
-- Table structure for table `seminars`
--

CREATE TABLE `seminars` (
  `seminar_id` int(11) NOT NULL,
  `seminar_name` text DEFAULT NULL,
  `seminar_desc` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `seminars`
--

INSERT INTO `seminars` (`seminar_id`, `seminar_name`, `seminar_desc`) VALUES
(34, 'Seminar Test', 'Seminar Test Description');

-- --------------------------------------------------------

--
-- Table structure for table `signatory`
--

CREATE TABLE `signatory` (
  `design_id` int(11) DEFAULT NULL,
  `signatory_id` int(11) NOT NULL,
  `signature_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `signatory`
--

INSERT INTO `signatory` (`design_id`, `signatory_id`, `signature_id`) VALUES
(20, 82, 8),
(20, 83, 9);

-- --------------------------------------------------------

--
-- Table structure for table `signature`
--

CREATE TABLE `signature` (
  `signature_id` int(11) NOT NULL,
  `signature_name` varchar(50) DEFAULT NULL,
  `signature_img` varchar(50) DEFAULT NULL,
  `signature_position` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `signature`
--

INSERT INTO `signature` (`signature_id`, `signature_name`, `signature_img`, `signature_position`) VALUES
(8, 'Maria Clara', '/images/signature-40138.png', 'Supervisor'),
(9, 'Juan Dela Cruz', '/images/signature-40139.png', 'Secretary'),
(10, 'Pedro Santos', '/images/signature-40121.png', 'Principal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `base_image`
--
ALTER TABLE `base_image`
  ADD PRIMARY KEY (`baseimage_id`);

--
-- Indexes for table `designs`
--
ALTER TABLE `designs`
  ADD PRIMARY KEY (`design_id`),
  ADD KEY `FK_designs_seminars` (`seminar_id`),
  ADD KEY `FK_designs_base_image` (`baseimage_id`),
  ADD KEY `FK_designs_logo` (`logo_id`);

--
-- Indexes for table `generated_certs`
--
ALTER TABLE `generated_certs`
  ADD PRIMARY KEY (`generated_id`),
  ADD KEY `FK_generated_certs_designs` (`seminar_id`) USING BTREE;

--
-- Indexes for table `login_credentials`
--
ALTER TABLE `login_credentials`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
  ADD PRIMARY KEY (`logo_id`);

--
-- Indexes for table `seminars`
--
ALTER TABLE `seminars`
  ADD PRIMARY KEY (`seminar_id`);

--
-- Indexes for table `signatory`
--
ALTER TABLE `signatory`
  ADD PRIMARY KEY (`signatory_id`),
  ADD KEY `FK_signatory_signature` (`signature_id`),
  ADD KEY `FK_signatory_designs` (`design_id`);

--
-- Indexes for table `signature`
--
ALTER TABLE `signature`
  ADD PRIMARY KEY (`signature_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `base_image`
--
ALTER TABLE `base_image`
  MODIFY `baseimage_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `designs`
--
ALTER TABLE `designs`
  MODIFY `design_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `generated_certs`
--
ALTER TABLE `generated_certs`
  MODIFY `generated_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `login_credentials`
--
ALTER TABLE `login_credentials`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
  MODIFY `logo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `seminars`
--
ALTER TABLE `seminars`
  MODIFY `seminar_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `signatory`
--
ALTER TABLE `signatory`
  MODIFY `signatory_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `signature`
--
ALTER TABLE `signature`
  MODIFY `signature_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `designs`
--
ALTER TABLE `designs`
  ADD CONSTRAINT `FK_designs_base_image` FOREIGN KEY (`baseimage_id`) REFERENCES `base_image` (`baseimage_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_designs_logo` FOREIGN KEY (`logo_id`) REFERENCES `logo` (`logo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_designs_seminars` FOREIGN KEY (`seminar_id`) REFERENCES `seminars` (`seminar_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `generated_certs`
--
ALTER TABLE `generated_certs`
  ADD CONSTRAINT `FK_generated_certs_seminars` FOREIGN KEY (`seminar_id`) REFERENCES `seminars` (`seminar_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `signatory`
--
ALTER TABLE `signatory`
  ADD CONSTRAINT `FK_signatory_designs` FOREIGN KEY (`design_id`) REFERENCES `designs` (`design_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_signatory_signature` FOREIGN KEY (`signature_id`) REFERENCES `signature` (`signature_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
