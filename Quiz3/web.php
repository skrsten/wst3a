<?php

use App\Http\Controllers\PostsController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
  return view('welcome');
});


//Route::get('/post', [PostsController::class, 'index']);

//Quiz3

//Route::get('/expost/{id}', [PostsController::class, 'create']);

Route::get('/customer/{CusID}/{Name}/{Address}', [OrderController::class, 'customer']);

Route::get('/item/{ItemNo}/{Name}/{Price}', [OrderController::class, 'item']);

Route::get('/order/{CusID}/{Name}/{OrderNo}/{Date}', [OrderController::class, 'order']);

Route::get('/orderDetails/{TransNo}/{OrderNo}/{ItemID}/{Name}/{Price}/{Qty}', [OrderController::class, 'orderDetails']);