<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function customer($CusID, $Name, $Address)
    {
        //
        return '<html>
        <body>    
        <form>
          <label for="id">CustomerID:</label>
          <input type="text" value=" '.$CusID.'"  readonly><br><br>
          <label for="country">Name:</label>  
          <input type="text"  value=" '.$Name.'"  readonly><br><br>
          <label for="country">Address: </label>  
          <input type="text" value=" '.$Address.'" readonly><br><br>     
        </form>      
        </body>
        </html>';
    }

    public function item($ItemNo, $Name, $Price)
    {
        //
        return '<html>
        <body>
      <form>
          <label for="id">ItemNo:</label>
          <input type="text" value=" '.$ItemNo.'"  readonly><br><br>
          <label for="country">Name:</label>  
          <input type="text"  value=" '.$Name.'"  readonly><br><br>
          <label for="country">Price: </label>  
          <input type="text" value=" '.$Price.'" readonly><br><br>         
        </form>       
        </body>
        </html>';
    }

    public function order($CusID, $Name, $OrderNo, $Price)
    {
        //
        return '<html>
        <body>      
        <form>
          <label for="id">CusID:</label>
          <input type="text" value=" '.$CusID.'"  readonly><br><br>
          <label for="country">Name:</label>  
          <input type="text"  value=" '.$Name.'"  readonly><br><br>
          <label for="country">OrderNo: </label>  
          <input type="text" value=" '.$OrderNo.'" readonly><br><br>
          <label for="country">Price: </label>  
          <input type="text" value=" '.$Price.'" readonly><br><br>
          
        </form>       
        </body>
        </html>';
    }


    public function orderDetails($TransNo , $OrderNo, $ItemID, $Name, $Price, $Qty)
    {
        //
        return '<html>
        <body>   
        <form>
          <label for="country">TransNo: </label>  
          <input type="text" value=" '.$TransNo.'" readonly><br><br>
          <label for="country">OrderNo: </label>  
          <input type="text" value=" '.$OrderNo.'" readonly><br><br>
          <label for="id">ItemID:</label>
          <input type="text" value=" '.$ItemID.'"  readonly><br><br>
          <label for="country">Name:</label>  
          <input type="text"  value=" '.$Name.'"  readonly><br><br>
          <label for="country">Price: </label>  
          <input type="text" value=" '.$Price.'" readonly><br><br>
          <label for="country">Qty: </label>  
          <input type="text" value=" '.$Qty.'" readonly><br><br>         
        </form>       
        </body>
        </html>';
    }


    public function create()
    {
        //
   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
