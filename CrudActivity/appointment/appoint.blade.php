<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <title>Psychologist</title>

</head>
<body>
    <div class="container mt-4">
    <nav class="navbar bg-info">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1">Psychotherapy Appointment</span>
    <ul class="nav justify-content-end">
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/UserHome">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/appoint">Psychologist</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/myappointment">My Appointment</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/login">Logout</a>
  </li>
    </ul>
  </div>
</nav>

<div>
  
<table class="table table-bordered table-info table-hover mt-4">
<thead class="text-center">
      <tr>
        <th >ID</th>
        <th>Psychologist</th>
        <th>Contact</th>
        <th>date</th>
        <th>From_time</th>
        <th>To_time</th>
        <th></th>
      </tr>
    </thead>
    
    <tbody class="text-center">
    @foreach ($psychologist as $staffs)
      <tr>
      <td>{{ $staffs->id }}</td>
      <td>{{ $staffs->psychologist }}</td>
      <td>{{ $staffs->phone }}</td>
      <td>{{ $staffs->date}}</td>
      <td>{{ $staffs->from_time}}</td>
      <td>{{ $staffs->to_time}}</td>
      <td class="text-center"><button type="button" class="btn btn-info" onclick="window.location.href='/create'">Set Appoinment</button></td>
      </tr>
      @endforeach
    </tbody>
  </table>


</div>
</body>
</html>