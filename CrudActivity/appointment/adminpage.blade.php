<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" 
integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">

<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <title>Home</title>

</head>
<body>
    <div class="container mt-4">
    <nav class="navbar bg-info">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1">Psychotherapy Appointment</span>
    <ul class="nav justify-content-end">
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/adminHome">Home</a>
  </li>
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/adminpage">Appointment/s</a>
  </li>
    <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/doctor">Psychologist</a>
  </li>
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/admin">Logout</a>
  </li>
    </ul>
  </div>
</nav>

<div>
  
<table class="table table-bordered table-info table-hover mt-4">
<thead class="text-center">
      <tr>
        <th >ID</th>
        <th>Client Name</th>
        <th>Email</th>
        <th>Contact</th>
        <th>Date</th>
        <th>Time</th>
        <th>Status</th>
        <th></th>
        <th></th>
        
      </tr>
    </thead>
    
    <tbody class="text-center">
    @foreach ($client as $clients)
      <tr>
      <td>{{ $clients->id }}</td>
      <td>{{ $clients->client }}</td>
      <td>{{ $clients->email }}</td>
      <td>{{ $clients->phone}}</td>
      <td>{{ $clients->date}}</td>
      <td>{{ $clients->time}}</td>
      <td>{{ $clients->status}}</td>
      <td class="text-center"><button type="button" id="btn" class="btn btn-success" onclick="window.location.href='/approved/{{ $clients->id }}'">Approve Appointment</button></td>
      <td class="text-center"><button type="button" id="" class="btn btn-success" onclick="window.location.href='/occupied/{{ $clients->id }}'">Occupied</button></td>
      <p id="alert"></p>
      </tr>
      @endforeach
    </tbody>
  </table>


</div>
</body>
<script>
  function myFunction() {
  document.getElementById("alert").innerHTML = "<div class='alert alert-success mt-2'>Appointment Approved</div>";
}

</script>
</html>