<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class patientController extends Controller
{
    public function patient() {
        $client = DB::select('select * from appoint');
        return view('appointment.adminpage',['client'=>$client]);
        }
    
    public function doctor() {
        $psychologist = DB::select('select * from sched');
        return view('appointment.doctor',['psychologist'=>$psychologist]);
        }
}
