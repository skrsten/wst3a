<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class viewController extends Controller
{
    //public function home() {
     //   return view('appointment.appoint');
     //   } 
    public function index() {
        $psychologist = DB::select('select * from sched');
        return view('appointment.appoint',['psychologist'=>$psychologist]);
        }

    public function client() {
        $client = DB::select('select * from appoint');
        return view('appointment.myappointment',['client'=>$client]);
        }
}
