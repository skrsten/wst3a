<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class doctorController extends Controller
{
    public function index() {
        $psychologist = DB::select('select * from sched');
        return view('appointment.doc_update',['psychologist'=>$psychologist]);
        }
        public function show($id) {
        $psychologist = DB::select('select * from sched where id = ?',[$id]);
        return view('appointment.doc_update',['psychologist'=>$psychologist]);
        }
        public function edit(Request $request,$id) {
        $psychologist = $request->input('doc');
        //$phone = $request->input('phone');
        //$schedule = $request->input('schedule');

        DB::update('update sched set psychologist = ? where id = ?',[$psychologist,$id]);
        return back()->with('success', "Successfully edited info");     
        }

       
}
