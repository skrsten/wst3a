<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class schedController extends Controller
{
    public function create() {
        return view('appointment.create');
        }

    public function insert(Request $request)
    {
        $client = $request->input('client');
        $email = $request->input('email');
        $phone = $request->input('phone');
        $date = $request->input('date');
        $time = $request->input('time');
        

        $data = [
            ['client'=>$client, 'email'=> $email, 'phone' => $phone, 'date' => $date,  'time'=>$time],
        ];
        
        DB::table('appoint')->insert($data); 
        return back()->with('success', "Successfully scheduled an appointment.");           
    }

   
}
