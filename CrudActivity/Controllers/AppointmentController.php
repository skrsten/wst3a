<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class AppointmentController extends Controller
{
    

  public function insertform() {
        return view('appointment.register');
        }

    public function register(Request $request)
    {
        $client_name = $request->input('client_name');
        $email = $request->input('email');
        $username = $request->input('username');
        $password = $request->input('password');

        $data = [
            ['client_name'=>$client_name, 'email'=> $email, 'username' => $username, 'password' => $password],
        ];
        
        DB::table('users')->insert($data); 
        return view('appointment.UserHome')->with('success', "Account successfully registered.");           
    }

    public function login() {
        return view('appointment.login');
        }

    public function loginUser() {
       $user = DB::select('select * from users');
       return view('appointment/UserHome',['users'=>$user]);
    }
    public function log() {
        return view('appointment.admin');
        }
    public function loginAdmin() {
        $admin = DB::select('select * from admin');
        return view('appointment/adminHome',['admin'=>$admin]);
     }

    public function Uhome() {
        return view('appointment.UserHome');
        }

    public function delete() {
        $client = DB::select('select * from appoint');
        return view('appointment.myappointment',['client'=>$client]);
            }

    public function destroy($id) {
        DB::delete('delete from appoint where id = ?',[$id]);
        return back()->with('success', "Deleted Successfully");     
            }
            public function deleted() {
                $psychologist = DB::select('select * from sched');
                return view('appointment.doctor',['psychologist'=>$psychologist]);
                    }
        
            public function destroyed($id) {
                DB::delete('delete from sched where id = ?',[$id]);
                return back()->with('success', "Deleted Successfully");     
                    }
     
    public function logout(Request $request) {
        Auth::logout();
        return redirect('appointment.login');
            }

    public function approved($id){
        $update = DB::table('appoint')
                    ->where('id',$id)
                    ->update(["status"=>"Appointment Approved"]);
        return back();
    }

    
    public function occupied($id){
        $update = DB::table('appoint')
                    ->where('id',$id)
                    ->update(["status"=>"Time slot is occupied, appoint another one"]);
        return back();
    }
}

