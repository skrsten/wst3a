<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class docInsertController extends Controller
{
    public function create() {
        return view('appointment.doc_insert');
        }

    public function insert(Request $request)
    {
        $doc = $request->input('doc');
        $phone = $request->input('phone');
        $date= $request->input('date');
        $from_time= $request->input('from_time');
        $to_time= $request->input('to_time');

        $data = [
            ['psychologist'=>$doc, 'phone' => $phone, 'date' => $date, 'from_time' => $from_time, 'to_time' => $to_time],
        ];
        
        DB::table('sched')->insert($data); 
        return back()->with('success', "Successfully added new psychologist");           
    }
}
