1. 	Place activity1.blade.php in the views folder within the resources folder,
	which is located in your laravel folder. (C:\xampp\htdocs\wst_blog\resources\views)

2. 	In the routes folder, add a route for the activity1.blade.php file in web.php.(C:\xampp\htdocs\wst_blog\routes) 
	--e.g--		
	Route::get('activity1', function () {
    	return view('activity1');
	});